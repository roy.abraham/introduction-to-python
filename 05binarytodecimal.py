import sys

def binarydecimal(user_binary):
    totalsum = 0
    previoussum = 0

    for number in user_binary:
        totalsum = (2 * previoussum) + int(number)
        str_previoussum = str(previoussum)
        str_number = str(number)
        print(("(2*0) + ") + (str_number) + (" = ") + (str_previoussum))
        previoussum = totalsum
        #print("(2*0) + 0 = 0")


    return totalsum


binarynumber = sys.argv[1]

bin_total = binarydecimal(binarynumber)
total = str(bin_total)
print(("Total: ") + (total))
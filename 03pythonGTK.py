# contains the Gtk library so we can have a GUI based program
import gi
# import sys so we can use the arguments passed to it from the commandline

import sys

script_name = sys.argv[0]
command = sys.argv[1]
time = int(sys.argv[2])
www = sys.argv[3]

# we say we require Gtk 3.0 for this Python program
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Pango
# subprocess let's us execute other programs from this one which is what we need to do in order to make the sysadmin scripts
import subprocess

class TechrichWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Areeb Test")
        self.set_size_request(500, 300)

        self.timeout_id = None

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)


        #our new output box for the program we can
        self.entry = Gtk.Entry()
        programarguments=[command, time, www]
        p=subprocess.Popen(programarguments, stdout=subprocess.PIPE)
        programoutput=str(p.communicate())
        self.entry.set_text(programoutput)
        vbox.pack_start(self.entry,True,True,0)


ay = TechrichWindow()
ay.connect("destroy", Gtk.main_quit)
ay.show_all()

Gtk.main()
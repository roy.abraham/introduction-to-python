import sys

def add(num1,num2):
    the_sum = int(num1) + int(num2)
    return the_sum

def min(num1,num2):
    the_minus = int(num1) - int(num2)
    return the_minus

def mult(num1,num2):
    the_multiply = int(num1)*int(num2)
    return the_multiply

def div(num1,num2):
    the_divide = int(num1)/int(num2)
    return the_divide

number1 = sys.argv[1]
operation = sys.argv[2]
number2 = sys.argv[3]

if operation == '+':
    answer = add(number1, number2)
    print(answer)

elif operation =='-':
    answer = min(number1, number2)
    print(answer)

elif operation =='X':
    answer = mult(number1, number2)
    print(answer)

elif operation =='/':
    answer = div(number1, number2)
    print(answer)

else:
    print("Please use +, -, X or /")



#answer = add(number1,number2)

#print(answer)
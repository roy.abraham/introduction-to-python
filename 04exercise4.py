# This Python file uses the following encoding: utf-8
article="""
My experience as an instructor at BCIT’s High Tech Professional, NASP Program

by Areeb Soo Yasir · Published October 1, 2018 · Updated October 10, 2018

I’ve done a lot of technical support in a hands-on way as a junior over a decade ago and I think this sets a person up for being able to train and teach.  As tech support we had to develop people skills and basically try to find the best way to teach the person and guide them.  I have trained many staff for my own companies and took a similar approach to how I was trained at BCIT (British Columbia Institute of Technology).

After setting up my companies with well trained educated staff, I took on the opportunity to teach at BCIT. It was a bit different then training staff in a datacenter or with hands on hardware and equipment. But it was a great experience.  In the end, teaching was more like consulting- educating and giving them support in a hands on environment as well as giving them an understanding of the material.

I viewed this opportunity to teach as a ‘win win’ situation. I’d be gaining more experience, as well as teaching this new generation of IT professionals good understanding of the IT world outside. In a notebook things work just great, but in the real world, stuff happens. And even if you do exactly as the book says, something pops up randomly that need a work around (like Vmware often needed). And I like to think, I gave my students a good lesson on the reality of IT- always keep learning.

I remembered back to the good ol’days of going to BCIT, I tried to apply the best of what I experienced as a former student so that this new generation of students could go out to the tech world confident. I had instructors who came in outside of class time to help and also ones who gave excellent career advice who still resonate with me today.  Those sorts of things make a difference and I believe in giving back in whatever shape or form possible.

Going back to the teaching, I had the privilege of teaching a course on Virtualization which specifically focused on VMWare vSphere 6.7, Cloud Architecture, High Availability, Network and Server Administration, Proxmox, KVM and I snuck in a bit of OpenStack in because they swam through the Proxmox material I created for them.
"""
#wordcount counts the words in the article
def wordcount(thearticle):
  # count the words here
  count = (len(thearticle.split()))
  return count

#charcount counts how many characters are in the article
def charcount(thearticle):
  #count the characters here
  count = (len(thearticle))
  return count


article_wordcount = wordcount(article)
print("word count = "+str(article_wordcount))

article_charcount = charcount(article)
print("char count = "+str(article_charcount))

#print("char count="+str(charcount(article)))



import random
def getAnswer(answerNumber):
    if answerNumber == 1:
        return 'It is certain'
    elif answerNumber == 2:
        return 'It is decidedly so'
    elif answerNumber == 3:
        return 'It is not ok'
    elif answerNumber == 4:
        return 'It is ok'
    elif answerNumber == 5:
        return 'It is smiley'
    elif answerNumber == 6:
        return 'It is happy'
    elif answerNumber == 7:
        return 'It is not'
    elif answerNumber == 8:
        return 'It is so'
    elif answerNumber == 9:
        return 'It is decidedly not'

r = random.randint(1,9)
fortune = getAnswer(r)
print(fortune)
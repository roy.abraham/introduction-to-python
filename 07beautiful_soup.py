# import libraries
import requests
from bs4 import BeautifulSoup

# specify the url
webpage = "https://www.memoryexpress.com/Category/HardDrives"

# query the website and return the html to the variable ‘page’
page = requests.get(webpage)

# parse the html using beautiful soup and store in variable `soup`
soup = BeautifulSoup(page.content, 'html.parser')

# find all img tags & print (unformatted)
#print(soup.find_all('img'))

#find all img tags and print the urls
#for url in soup.find_all("img"):
#    print(url)
#    print(url.get('src')) # Collect product image links


# testing
#for paragraph in soup.find_all('p'):
#       print(paragraph.text)
# Get text from webpage
#print(soup.get_text())
#print(soup.find_all('p'))

for price in soup.find_all("div", "c-shca-icon-item__summary-list"):
    print(price.text)
#    img =
#    name =
#    price =
#    print(url.get('src')) # Collect product image links
